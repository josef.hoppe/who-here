/*
Who Here
Copyright (C) 2020  Josef Hoppe

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published 
by the Free Software Foundation; either version 2 of the License, 
or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

package io.hoppe.whohere.data

import androidx.annotation.Keep
import com.fasterxml.jackson.annotation.JsonIgnoreProperties

@JsonIgnoreProperties("\$schema")
@Keep
data class ApplicationData(
    /**
     * Version of the data. Incremented when new questions are added
     */
    val ver: Int = 0,
    /**
     * Minimal required version of the App
     */
    val appver: Appver = Appver(),
    /**
     * All Questions
     */
    val data: List<Question> = listOf(),
    /**
     * How to translate the categories
     */
    val categoryTranslations: Map<String, Map<String, String>> = mapOf()
) {
    companion object {
        fun categoryActiveDefault(category: String): Boolean {
            return category != "Spicy"
        }
    }
}